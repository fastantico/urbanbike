//
//  Metodos.m
//  UrbanBike
//
//  Created by Manuel  Cervantes on 24/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import "Metodos.h"
#import <MapKit/MapKit.h>
@interface Metodos ()

@end

@implementation Metodos

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)locationManagers:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    
    double lat = newLocation.coordinate.latitude;
    double longs = newLocation.coordinate.longitude;
    
    

    NSLog(@"%f %f",lat,longs);


}

@end
