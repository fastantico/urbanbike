//
//  AppDelegate.m
//  UrbanBike
//
//  Created by Manuel  Cervantes on 07/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "Principal.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    

    //inicializacion de parse
    [Parse setApplicationId:@"kG1I7S91t73okWwrKoyffRoVKGbkGMxjcn9uKnOc"
                  clientKey:@"a56ZchJzaIUXxZUNVnRIof2gYnFR6bHAOL3Xgq1x"];
    
    
    
    //inicializacion de facebook
    
    [PFFacebookUtils initializeFacebook];

    // Override point for customization after application launch.
      return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [PFFacebookUtils handleOpenURL:url];
}

							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSession.activeSession handleDidBecomeActive];

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
    [FBSession.activeSession close];

    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
