//
//  ViewController.h
//  UrbanBike
//
//  Created by Manuel  Cervantes on 07/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIdicator;
- (IBAction)Login;


@end
