//
//  Principal.m
//  UrbanBike
//
//  Created by Manuel  Cervantes on 17/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import "Principal.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Metodos.h"
#import "math.h"
#import "SVProgressHUD.h"

double lat1=0,lat2=0,dist = 0,dist_acumulada=0;
//=========flags=======
int flag=0,cont=0,marca=0,flag2=0,suma=0;
NSString *latitud_actual,*longitud_actual,*lat_temp1,*lat_temp2, *long_temp1, *long_temp2;
NSMutableArray *latitudes,*longitudes,*longitudes,*latitudes_conjunto,*longitudes_conjunto;
NSString *valor2;

@interface Principal ()

@end
@implementation Principal
{
    MKPolyline * routeLine;

}
@synthesize TopLayer = _TopLayer;
@synthesize layerPosition = _layerPosition;
@synthesize mapView=_mapView;
@synthesize locationManager,velocimetro,metodos,distancia;



#define VIEW_HIDDEN 100
#define DEGREES_TO_RADIANS(x) (M_PI * x / 180.0)
#define RADIANS_TO_DEGREES(x) (180 * x / M_PI)


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)location{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // mientras te mueves
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
    
    /*velocimetro*/
    
    
    NSLog(@"locations");

    

}

#define lat_prueba =
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation

{
  //  CLLocation * prueb1 = [[CLLocation alloc] initWithLatitude:(29.4179305832078) longitude:(-98.18151)];
    //CLLocation * prueb2 = [[CLLocation alloc] initWithLatitude:(29.4179405832078) longitude:(-98.18151)];

   
    
        double lat = newLocation.coordinate.latitude;
   latitud_actual = [NSString stringWithFormat:@"%f", lat];
    double longs = newLocation.coordinate.longitude;
    longitud_actual = [NSString stringWithFormat:@"%f", longs ];
    if (flag==1){
        [latitudes addObject:latitud_actual];
        [latitudes_conjunto addObject:latitud_actual];
        [longitudes addObject:longitud_actual];
        [longitudes_conjunto addObject:longitud_actual];

        NSLog(@"%@",latitudes_conjunto);
        NSLog(@"%@",longitudes_conjunto);
        if ([latitudes count]==cont+3) {
            for (marca;marca<flag2+2;marca++)
            {
                if (marca+1 < flag2+2) {
                    suma= marca+(marca+1);
                    
                    lat_temp1 = [latitudes_conjunto objectAtIndex:marca];
                    lat_temp2 = [latitudes_conjunto objectAtIndex:(marca+1)];
                  
                    long_temp1 = [longitudes_conjunto objectAtIndex:marca];
                    long_temp2 =[longitudes_conjunto objectAtIndex:marca+1];
                    
                    long latitude1 =lat_temp1;
                    long longitude1 = lat_temp2;
                    long latitude2 = long_temp1;
                    long longitude2 = long_temp2;
                    CLLocation * prueb1 = [[CLLocation alloc] initWithLatitude:(latitude1) longitude:(longitude1)];
                    CLLocation * prueb2 = [[CLLocation alloc] initWithLatitude:(latitude2) longitude:(longitude2)];
                    
                    CLLocationDistance distanchia = [prueb1 distanceFromLocation:prueb2];
                    double algo = distanchia * .001;
                    NSLog(@"funcion %f",algo);

                    double tetha = [long_temp1 doubleValue] - [long_temp2 doubleValue ];
                    
                    NSLog(@"theta %f",tetha);
                    NSLog(@"valor en radianes %f",[lat_temp1 doubleValue ]);

                    NSLog(@"valor en radianes %f",[lat_temp2 doubleValue ]);
                    
                    double lat1 = DEGREES_TO_RADIANS([lat_temp1 doubleValue]);
                    
                    NSLog(@"lat1 %f",lat1);
                    double lat2 = DEGREES_TO_RADIANS([lat_temp2 doubleValue]);
                    
                    NSLog(@"lat2 %f",lat2);
                    
                     dist =  sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(DEGREES_TO_RADIANS( tetha));
                    
                    NSLog(@"dist %f",dist);
                    
                    
                    dist = acos(dist);
                    dist = RADIANS_TO_DEGREES(dist);
                    NSLog(@"radianes a grados %f",dist);
                    dist = dist * 60  * 1.1515;
                    NSLog(@"dist %f",dist);

                    
                    dist = dist * 1.609344;
                    dist_acumulada  = dist_acumulada +(dist * 1000);
                    NSLog(@"dist %f",dist);

                    distancia.text = [NSString stringWithFormat:@"%f",dist_acumulada];
                    
                    }
                 
                    [latitudes_conjunto addObject:latitud_actual];

                }
                cont = marca;
            
            }
            
            flag2 = cont;
            
                   
        }
        MKUserLocation *userLocation = _mapView.userLocation;
        MKCoordinateRegion region =
        MKCoordinateRegionMakeWithDistance (
                                            userLocation.location.coordinate, 350, 350);
        [_mapView setRegion:region animated:NO];


    }
    



- (IBAction)zoomIn:(id)sender {
    MKUserLocation *userLocation = _mapView.userLocation;
    MKCoordinateRegion region =
    MKCoordinateRegionMakeWithDistance (
                                        userLocation.location.coordinate, 350, 350);
    [_mapView setRegion:region animated:NO];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.layerPosition = self.TopLayer.frame.origin.x;
    [self location];
    _mapView.showsUserLocation= YES;
    longitudes =  [[NSMutableArray alloc] init];
    latitudes =  [[NSMutableArray alloc] init];
    latitudes_conjunto = [[NSMutableArray alloc] init];
    
    
    
  
    
    
      //  [_mapView setRegion:region animated:YES];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)toggleLayer:(id)sender {
    if (self.layerPosition == VIEW_HIDDEN) {
        [self animatedLayerToPoint:0];
    }
    else
    {
        [self animatedLayerToPoint:VIEW_HIDDEN];
    }
}

-(void)animatedLayerToPoint:(CGFloat)x{
    
[UIView animateWithDuration:0.3
                      delay:0
                    options:UIViewAnimationCurveEaseOut
                 animations:^{
                     CGRect frame = self.TopLayer.frame;
                     frame.origin.x = x;
                     self.TopLayer.frame = frame;
                 }
                 completion:^(BOOL finished) {
                     self.layerPosition = self.TopLayer.frame.origin.x;

                 }];

}






- (IBAction)panLayer:(UIPanGestureRecognizer *)pan
{
    
    if(pan.state == UIGestureRecognizerStateChanged)
    {
        CGPoint point = [pan translationInView:self.TopLayer];
        CGRect frame = self.TopLayer.frame;
        frame.origin.x = self.layerPosition + point.x;
        if(frame.origin.x < 0) frame.origin.x = 0 ;
        self.TopLayer.frame = frame;
        
    }
    
    if(pan.state == UIGestureRecognizerStateEnded)
    {
        if(self.TopLayer.frame.origin.x >=  160)
        {
            [self animatedLayerToPoint:0];
            
        }
        else{
            [self animatedLayerToPoint: VIEW_HIDDEN];
        }
            
    
    }
    
}




-(void)rotar: (int )d{
   
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: DEGREES_TO_RADIANS(d)];
    rotationAnimation.duration = .3;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [velocimetro.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    
}


        
        
    

/*
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];

        [UIView setAnimationDuration:1.3];
   // velocimetro.layer.anchorPoint = CGPointMake(0.50, 1.62);
    [velocimetro setCenter:_TopLayer.center ] ;
    
  velocimetro.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(10));

        [UIView commitAnimations];*/
    

-(void)rota2{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:1.3];
    velocimetro.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
    [UIView commitAnimations];

    
}


-(IBAction)start
{
    
    //longitud_actual
    //latitud_actual
    
    if (flag==0) {
        flag=1;
        [self trazar_ruta];
        [self velocidad_distancia];
        
    }
    else
        flag = 0;
    
    
    
    


}
- (IBAction)stop:(id)sender {
    
    
    flag = 0;
    
    [latitudes removeAllObjects];
    
    NSLog(@"%d", [latitudes count]);
    
}


-(void)trazar_ruta{
    
  



}

-(void) velocidad_distancia{
    
    

    
    
}

-(void) tiempo{
    
}



    

@end
