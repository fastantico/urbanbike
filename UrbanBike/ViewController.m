//
//  ViewController.m
//  UrbanBike
//
//  Created by Manuel  Cervantes on 07/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import "ViewController.h"
#import "Principal.h"
#import <Parse/Parse.h>




@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Facebook Profile";
    // Check if user is cached and linked to Facebook, if so, bypass login
    
}
#pragma mark - Login mehtods
    
    /* Login to facebook method */
    
    
-(void)viewDidAppear:(BOOL)animated
{
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
      [self cambiarPantalla];
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
}
    



- (void) cambiarPantalla{

    Principal *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"menu_principal"];
    controller.modalPresentationStyle = UIModalPresentationFormSheet;
    
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:controller animated:YES completion:nil];

    
    
}

- (IBAction)Login {
    NSLog(@"Bundle ID: %@",[[NSBundle mainBundle] bundleIdentifier]);

    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location",@"email",@"user_friends"];
    
    // Login PFUser using facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [_activityIdicator stopAnimating]; // Hide loading indicator
        if (!user) {

            if (!error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:@"Uh oh. The user cancelled the Facebook login." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                NSLog(@"%@",alert);

               [alert show];
                
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:[error description] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                NSLog(@"%@",alert);
                [alert show];
            }
        } else if (user.isNew) {
            [self cambiarPantalla];
            
            
        } else {
        [self cambiarPantalla];
        }
    }];
    
    [_activityIdicator startAnimating]; // Show loading indicator until login is finished
    
}

-(IBAction)regresar
{
    [self dismissViewControllerAnimated:YES  completion:nil];
}



    @end
