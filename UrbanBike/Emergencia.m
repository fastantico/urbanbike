//
//  Emergencia.m
//  UrbanBike
//
//  Created by Manuel  Cervantes on 12/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import "Emergencia.h"
#import <AddressBook/AddressBook.h>
#import "SVProgressHUD.h"

@interface Emergencia ()

@end


@implementation Emergencia

@synthesize mapView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self lista_contactos];
    //inicializacion de parse id & key
   
    
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(37.501364, -122.182817);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.906448, 0.878906);
    self.mapView.region = MKCoordinateRegionMake(centerCoordinate, span);
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)regresar
{
    [self dismissViewControllerAnimated:YES  completion:nil];
}


//funcion obtener lista de contactos
-(void)lista_contactos{


    
}


-(void)viewDidAppear:(BOOL)animated{
    
    
    [super viewDidAppear:animated];
    
    // ---- Start searching ----
    
    [SVProgressHUD showWithStatus:@"Searching..."
                         maskType:SVProgressHUDMaskTypeGradient];
    
    // San Francisco Caltrain Station
    CLLocationCoordinate2D fromCoordinate = CLLocationCoordinate2DMake(37.7764393,
                                                                       -122.39432299999999);\
    // Mountain View Caltrain Station
    CLLocationCoordinate2D toCoordinate   = CLLocationCoordinate2DMake(37.393879,
                                                                       -122.076327);
    
    MKPlacemark *fromPlacemark = [[MKPlacemark alloc] initWithCoordinate:fromCoordinate
                                                       addressDictionary:nil];
    
    MKPlacemark *toPlacemark   = [[MKPlacemark alloc] initWithCoordinate:toCoordinate
                                                       addressDictionary:nil];
    
    MKMapItem *fromItem = [[MKMapItem alloc] initWithPlacemark:fromPlacemark];
    
    MKMapItem *toItem   = [[MKMapItem alloc] initWithPlacemark:toPlacemark];
    
    [self findDirectionsFrom:fromItem
                          to:toItem];
    
}


// =============================================================================
#pragma mark - Private

- (void)findDirectionsFrom:(MKMapItem *)source
                        to:(MKMapItem *)destination
{
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    request.source = source;
    request.destination = destination;
    request.requestsAlternateRoutes = YES;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         
         [SVProgressHUD dismiss];
         
         if (error) {
             
             NSLog(@"el error ess   error:%@", error);
         }
         else {
             
             NSLog(@"rutas plaksd");
             MKRoute *route = response.routes[1];
             
             [self.mapView addOverlay:route.polyline];
         }
     }];
}


// =============================================================================
#pragma mark - MKMapViewDelegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView
            rendererForOverlay:(id<MKOverlay>)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.lineWidth = 5.0;
    renderer.strokeColor = [UIColor purpleColor];
    return renderer;
}



@end
