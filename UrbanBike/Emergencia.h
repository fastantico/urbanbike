//
//  Emergencia.h
//  UrbanBike
//
//  Created by Manuel  Cervantes on 12/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface Emergencia : UIViewController <MKMapViewDelegate>
@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@end
