//
//  Principal.h
//  UrbanBike
//
//  Created by Manuel  Cervantes on 17/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Metodos.h"



@interface Principal : UIViewController <CLLocationManagerDelegate>{
    IBOutlet UIBarButtonItem *slidhid;

}
@property (strong, nonatomic) IBOutlet UIView *TopLayer;
@property (strong, nonatomic) IBOutlet UIImageView *velocimetro;
@property (nonatomic) CGFloat layerPosition;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic,retain) Metodos *metodos;
@property (nonatomic,retain) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UILabel *lblLatitud;
@property (weak, nonatomic) IBOutlet UILabel *lblLongitud;
@property (strong, nonatomic) IBOutlet UILabel *distancia;
-(IBAction)rota:(id)sender;

-(IBAction)Comineza_trayecto:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;
@end
