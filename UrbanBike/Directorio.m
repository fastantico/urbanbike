//
//  Directorio.m
//  UrbanBike
//
//  Created by Manuel  Cervantes on 12/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import "Directorio.h"

@interface Directorio ()

@end

@implementation Directorio

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)regresar
{
    [self dismissViewControllerAnimated:YES  completion:nil];
}


@end
