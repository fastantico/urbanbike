//
//  Metodos.h
//  UrbanBike
//
//  Created by Manuel  Cervantes on 24/09/13.
//  Copyright (c) 2013 Manuel  Cervantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface Metodos : UIViewController
-(void)locationManagers:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;
@end
